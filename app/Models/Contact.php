<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Sushi\Sushi;

class Contact extends Model
{
    use Sushi;

    protected $rows = [
        ['id' => 1, 'name' => 'Ivanko', 'mob_phone' => '+380887771122'],
        ['id' => 2, 'name' => 'Ebanko', 'mob_phone' => '+380883334455'],
        ['id' => 3, 'name' => 'Poebko', 'mob_phone' => '+380881112233'],
    ];
}
