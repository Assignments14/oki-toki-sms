<?php

declare(strict_types=1);

namespace App\DTO;

final class OpResult {
    public function __contstruct(
        public int $code,
        public string $message,
    )
    {
        
    }
}