<?php

declare(strict_types=1);

namespace App\UseCase;

use App\Contracts\GateRepository;
use App\Contracts\SmsSender;
use App\DTO\GateParams;
use App\DTO\OpResult;

final class SendSms {

    public function __construct(
        private GateRepository $repository, 
        private SmsSender $sender
    ) {}
    
    public function text(string $text, int $gateId, GateParams $gateParams): OpResult
    {
        return new OpResult();
    }
}