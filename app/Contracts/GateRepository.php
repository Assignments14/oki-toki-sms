<?php

namespace App\Contracts;

use App\Models\Contact;

interface GateRepository {
    public function getContact(int $id): Contact;
}