<?php

namespace App\Contracts;

use App\DTO\OpResult;

interface SmsSender {
    public function send(): OpResult;
}